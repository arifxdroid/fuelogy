package ideology;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.thingworx.communications.client.ClientConfigurator;
import com.thingworx.communications.client.ConnectedThingClient;
import com.thingworx.communications.common.SecurityClaims;
import com.thingworx.relationships.RelationshipTypes.ThingworxEntityTypes;
import com.thingworx.types.InfoTable;
import com.thingworx.types.collections.ValueCollection;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Sample extends Application implements Runnable{

	private String uri = "ws://23.228.149.221:80/Thingworx/WS";
	private String appKey = "0001f50b-fc24-4649-b7df-8aba824bcad6";
	private ClientConfigurator config;
	private ConnectedThingClient client;
	SecurityClaims claims;
	ListView<HBoxCell> listView;
	List<HBoxCell> list;

	int DRIVING = 1;
	int NOTDRIVING = 2;
	int OFFDUTY = 3;
	int SLEEPING = 4;

	ObservableList<DriverStatus> driverStatus = FXCollections.observableArrayList();

	public static void main(String[] args) {
		launch(args);
	}

	private ClientConfigurator connectingToCloud() {
		config = new ClientConfigurator();
		config.setUri(uri);
		config.setReconnectInterval(15);
		claims = SecurityClaims.fromAppKey(appKey);
		config.setSecurityClaims(claims);
		config.ignoreSSLErrors(true);
		config.setConnectTimeout(10000);

		return config;
	}

	@Override
	public void start(Stage primaryStage) throws FileNotFoundException {

		primaryStage.setTitle("Fuelogy");
		Group root = new Group();

		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					client = new ConnectedThingClient(connectingToCloud());
					client.start();

				} catch (Exception e) {
					e.printStackTrace();
					// Toast.makeText(getApplicationContext(), "" + e,
					// Toast.LENGTH_LONG).show();
				}

			}
		}).start();
		

		StackPane pan = new StackPane();
		pan.setLayoutX(700);
		pan.setLayoutY(80);
		pan.setPrefHeight(500);
		pan.setPrefWidth(300);
		Canvas canvas = new Canvas(700, 700);
		GraphicsContext gc = canvas.getGraphicsContext2D();

		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					Thread.sleep(12000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				drawShapes(gc);
			}
		}).start();

		listView = new ListView<HBoxCell>();
		listView.setPrefSize(300, 400);
		listView.setLayoutY(100);
		listView.isEditable();
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					Thread.sleep(15000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
				genarateList();
			}
		}).start();

		Label txtLabel = new Label();
		txtLabel.setLayoutX(780);
		txtLabel.setLayoutY(30);
		txtLabel.setText("Remarks");
		txtLabel.setFont(Font.font(30));

		root.getChildren().add(txtLabel);
		root.getChildren().add(pan);
		root.getChildren().add(canvas);
		pan.getChildren().add(listView);
		primaryStage.setScene(new Scene(root));
		primaryStage.setWidth(1050);
		primaryStage.show();
	}

	private void genarateList() {
		
		//getValuesFromCloud();
		list = new ArrayList<>();
		int itarate = 65;
		for (int i = 0; i < driverStatus.size(); i++) {

			String state = null;
			String color;
			String labelText = null;

			switch (driverStatus.get(i).getDrivingState()) {
			case 1:
				state = "DRIVING";
				color = ColorCustom.DRIVING_COLOR_HEX;
				break;
			case 2:
				state = "NOT DRIVING";
				color = ColorCustom.NOT_DRIVING_COLOR_HEX;
				break;
			case 3:
				state = "OFF DUTY";
				color = ColorCustom.OFF_DUTY_COLOR_HEX;
				break;
			case 4:
				state = "SLEEPING";
				color = ColorCustom.SLEEP_COLOR_HEX;

			default:
				state = "OFF DUTY";
				color = ColorCustom.OFF_DUTY_COLOR_HEX;
				break;
			}

			if (itarate <= 90) {
				labelText = String.valueOf((char) itarate);
				itarate++;
			} else {
				itarate = 65;
				labelText = String.valueOf((char) itarate);
				itarate++;
			}

			list.add(new HBoxCell(labelText, color, "Start Time: " + driverStatus.get(i).getStartTime(),
					"State: " + state));
		}
		ObservableList<HBoxCell> myObservableList = FXCollections.observableList(list);
		listView.setItems(myObservableList);

	}

	private void getValuesFromCloud() {
		ValueCollection params = new ValueCollection();
		InfoTable result;
		try {
			result = client.invokeService(ThingworxEntityTypes.Things, "FuelogyData", "returnDriver", params, 5000);
			if (!result.isEmpty()) {

				for (int i = 0; i < result.getRowCount(); i++) {

					String time = result.getRow(i).getStringValue("StartTime");
					String status = result.getRow(i).getStringValue("DrivingState");
					driverStatus.add(new DriverStatus(time, returnStatus(status)));
					System.out.println(time + " ..........and ....... " + status);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			client.disconnect();
			// client.shutdown();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void insertValues() {
		try {
			File inputFile = new File("src/ideology/Status.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			inputFile.canRead();
			System.out.println(inputFile.canRead());
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("DriverStatus");
			System.out.println("----------------------------");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				System.out.println("\nCurrent Element :" + nNode.getNodeName());
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					System.out.println(
							"Start Time : " + eElement.getElementsByTagName("StartTime").item(0).getTextContent());
					System.out.println(
							"Driver State : " + eElement.getElementsByTagName("DrivingState").item(0).getTextContent());
					String time = eElement.getElementsByTagName("StartTime").item(0).getTextContent();
					String status = eElement.getElementsByTagName("DrivingState").item(0).getTextContent();
					driverStatus.add(new DriverStatus(time, returnStatus(status)));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void dynamicValueDisplay(GraphicsContext gc) {
		DriverStatus startingStatus = new DriverStatus("12am", SLEEPING);
		int i = 0;
		float startPoint = 90.0f;
		float endPoint = 0;
		boolean emptyArrayList = (driverStatus.size() == 0);
		boolean twoValueInArrayList = (driverStatus.size() >= 2);
		boolean lastElementOfArrayList;

		if (!emptyArrayList) {
			if (twoValueInArrayList) {
				startingStatus.setEndPoint(driverStatus.get(0).getStartPoint());
			} else {
				// driverStatus.get(0).setEndPoint(-90f);
			}
			gc.setFill(startingStatus.getColor());
			gc.fillArc(200, 200, 300, 300, startPoint = startingStatus.getStartPoint(),
					endPoint = -startingStatus.getEndPoint(), ArcType.ROUND);

			System.out.println("start point 1- " + startPoint);
			System.out.println("end point 1- " + endPoint);

			for (i = 0; i < driverStatus.size(); i++) {
				lastElementOfArrayList = (i == driverStatus.size() - 1);

				if (!lastElementOfArrayList) {
					driverStatus.get(i)
							.setEndPoint(driverStatus.get(i + 1).getStartPoint() - driverStatus.get(i).getStartPoint());
				} else {
					// driverStatus.get(i).setEndPoint(360f);

				}

				gc.setFill(driverStatus.get(i).getColor());
				gc.fillArc(200, 200, 300, 300, startPoint = startPoint + endPoint,
						endPoint = -driverStatus.get(i).getEndPoint(), ArcType.ROUND);

				System.out.println("start point- " + startPoint);
				System.out.println("end point- " + endPoint);
			}
		}

	}

	private void drawShapes(GraphicsContext gc) {

		int centerPointX = 350;
		int centerPointY = 350;
		Font quartzFont;

		// insertValues();
		getValuesFromCloud();

		// Full Background color
		gc.setFill(Color.WHITE);
		gc.fillRect(0, 0, 500, 500);

		// border draw black
		gc.strokeOval(200, 200, 300, 300);

		// default color, off duty
		gc.setFill(ColorCustom.OFF_DUTY_COLOR);
		gc.fillOval(200, 200, 300, 300);

		// -----------Dynamic value starts here------------------------

		dynamicValueDisplay(gc);

		// -----------Dynamic value ends here----------------------------

		// Center color white
		gc.setFill(Color.WHITE);
		gc.fillOval(225, 225, 300 - 50, 300 - 50);
		// Center border line
		gc.strokeOval(225, 225, 300 - 50, 300 - 50);

		// *************DRAW LINE TICK*******************
		// Line tick that i mean here is, short line to indicate second.

		for (int i = 1; i <= 360; i++) {
			int tickX;
			int tickY;

			int tickXb;
			int tickYb;

			tickX = (int) (Math.cos(i * Math.PI / 12 - Math.PI / 2) * 150 + centerPointX);
			tickY = (int) (Math.sin(i * Math.PI / 12 - Math.PI / 2) * 150 + centerPointY);

			tickXb = (int) (Math.cos(i * Math.PI / 12 - Math.PI / 2) * 156 + centerPointX);
			tickYb = (int) (Math.sin(i * Math.PI / 12 - Math.PI / 2) * 156 + centerPointY);
			gc.strokeLine(tickXb, tickYb, tickX, tickY);

			tickX = (int) (Math.cos(i * Math.PI / 24 - Math.PI / 2) * 150 + centerPointX);
			tickY = (int) (Math.sin(i * Math.PI / 24 - Math.PI / 2) * 150 + centerPointY);

			tickXb = (int) (Math.cos(i * Math.PI / 24 - Math.PI / 2) * 152 + centerPointX);
			tickYb = (int) (Math.sin(i * Math.PI / 24 - Math.PI / 2) * 152 + centerPointY);
			gc.strokeLine(tickXb, tickYb, tickX, tickY);

		}
		// *************DRAW LINE TICK*******************

		// Draw number 12 to 1
		quartzFont = new Font("Verdana", 10);
		gc.setFont(quartzFont);
		gc.strokeText("12am", centerPointX - 15, centerPointY - 160);
		gc.strokeText("1", centerPointX + 40, centerPointY - 155);
		gc.strokeText("2", centerPointX + 78, centerPointY - 139);
		gc.strokeText("3", centerPointX + 111, centerPointY - 113);
		gc.strokeText("4", centerPointX + 137, centerPointY - 79);
		gc.strokeText("5", centerPointX + 153, centerPointY - 40);

		gc.strokeText("6am", centerPointX + 160, centerPointY + 2);
		gc.strokeText("7", centerPointX + 153, centerPointY + 45);
		gc.strokeText("8", centerPointX + 138, centerPointY + 83);
		gc.strokeText("9", centerPointX + 112, centerPointY + 118);
		gc.strokeText("10", centerPointX + 73, centerPointY + 146);
		gc.strokeText("11", centerPointX + 36, centerPointY + 161);

		gc.strokeText("12pm", centerPointX - 15, centerPointY + 165);
		gc.strokeText("1", centerPointX - 47, centerPointY + 160);
		gc.strokeText("2", centerPointX - 85, centerPointY + 145);
		gc.strokeText("3", centerPointX - 118, centerPointY + 119);
		gc.strokeText("4", centerPointX - 144, centerPointY + 85);
		gc.strokeText("5", centerPointX - 160, centerPointY + 45);

		gc.strokeText("6pm", centerPointX - 182, centerPointY + 2);
		gc.strokeText("7", centerPointX - 159, centerPointY - 40);
		gc.strokeText("8", centerPointX - 145, centerPointY - 77);
		gc.strokeText("9", centerPointX - 118, centerPointY - 113);
		gc.strokeText("10", centerPointX - 88, centerPointY - 140);
		gc.strokeText("11", centerPointX - 49, centerPointY - 155);
	}

	public static class HBoxCell extends HBox {

		Label labelTxt = new Label();
		VBoxCell vCell;

		HBoxCell(String labelText, String labelBgColor, String startTime, String drivingStatus) {
			super();

			labelTxt.setText(labelText);
			labelTxt.setPrefWidth(20);
			labelTxt.setAlignment(Pos.CENTER);
			labelTxt.setStyle("-fx-background-color: " + labelBgColor + ";");
			HBox.setHgrow(labelTxt, Priority.ALWAYS);

			vCell = new VBoxCell(startTime, drivingStatus);
			this.setStyle("-fx-border-insets: 5; -fx-border-width: 1;-fx-border-color: #000000;");
			// -fx-border-style: line;
			this.setSpacing(10);
			this.getChildren().addAll(labelTxt, vCell);

		}
	}

	public static class VBoxCell extends VBox {

		Label label = new Label();
		Label labelStatus = new Label();

		VBoxCell(String startTime, String drivingStatus) {
			super();

			label.setText(startTime);
			// label.setFont(Font.font(15));
			// label.setMaxWidth(Double.MAX_VALUE);
			HBox.setHgrow(label, Priority.ALWAYS);

			labelStatus.setText(String.valueOf(drivingStatus));
			// labelStatus.setFont(Font.font(15));
			// HBox.setHgrow(label, Priority.ALWAYS);
			labelStatus.setAlignment(Pos.BOTTOM_LEFT);

			this.setSpacing(5);
			this.getChildren().addAll(label, labelStatus);

		}
	}

	public int returnStatus(String str) {
		int state = 0;
		str = str.toUpperCase();
		switch (str) {

		case "DRIVING":
			state = DRIVING;
			break;

		case "NOTDRIVING":
			state = NOTDRIVING;
			break;

		case "OFFDUTY":
			state = OFFDUTY;
			break;

		case "SLEEPING":
			state = SLEEPING;
			break;

		default:
			state = OFFDUTY;
			break;
		}
		return state;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Thread.sleep(12000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}